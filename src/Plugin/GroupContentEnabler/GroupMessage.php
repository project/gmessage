<?php

namespace Drupal\gmessage\Plugin\GroupContentEnabler;

use Drupal\group\Plugin\GroupContentEnablerBase;

/**
 * Group content enabler for message entities.
 *
 * @GroupContentEnabler(
 *  id = "group_message",
 *  label = @Translation("Message"),
 *  description = @Translation("Adds messages to groups"),
 *  entity_type_id = "message",
 *  entity_access = "TRUE",
 *  reference_label = @Translation("ID"),
 *  reference_description = @Translation("The ID of the message to add to the group"),
 *  handlers = {
 *    "permission_provider" = "Drupal\group\Plugin\GroupContentPermissionProvider",
 *  },
 *  deriver = "\Drupal\gmessage\Plugin\Group\Relation\GroupMessageDeriver"
 * )
 */
class GroupMessage extends GroupContentEnablerBase {
}
