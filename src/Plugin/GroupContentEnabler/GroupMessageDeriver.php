<?php

namespace Drupal\gmessage\Plugin\GroupContentEnabler;

use Drupal\message\Entity\MessageTemplate;
use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Group content deriver for message entities.
 */
class GroupMessageDeriver extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (MessageTemplate::loadMultiple() as $name => $message_type) {
      $label = $message_type->label();

      $this->derivatives[$name] = [
        'entity_bundle' => $name,
        'label' => t('Group message (@label)', ['@label' => $label]),
        'description' => t('Adds %type messages to groups.', ['%type' => $label]),
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
