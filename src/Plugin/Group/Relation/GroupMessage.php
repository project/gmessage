<?php

namespace Drupal\gmessage\Plugin\Group\Relation;

use Drupal\group\Plugin\Group\Relation\GroupRelationBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a group relation type for nodes.
 *
 * @GroupRelationType(
 *   id = "group_message",
 *   label = @Translation("Message"),
 *   description = @Translation("Adds messages to groups"),
 *   entity_type_id = "message",
 *   entity_access = TRUE,
 *   reference_label = @Translation("ID"),
 *   reference_description = @Translation("The ID of the message to add to the group"),
 *   deriver = "Drupal\gmessage\Plugin\Group\Relation\GroupMessageDeriver",
 * )
 */
class GroupMessage extends GroupRelationBase {
}
