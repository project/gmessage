<?php

namespace Drupal\gmessage\Plugin\Group\Relation;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeInterface;
use Drupal\message\Entity\MessageTemplate;

class GroupMessageDeriver extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    assert($base_plugin_definition instanceof GroupRelationTypeInterface);

    $this->derivatives = [];

    foreach (MessageTemplate::loadMultiple() as $name => $message_type) {
      $label = $message_type->label();

      $this->derivatives[$name] = clone $base_plugin_definition;
      $this->derivatives[$name]->set('entity_bundle', $name);
      $this->derivatives[$name]->set('label', t('Group message (@label)', ['@label' => $label]));
      $this->derivatives[$name]->set('description', t('Adds %type messages to groups.', ['%type' => $label]));
    }

    return $this->derivatives;
  }

}
